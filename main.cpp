#include "App/App.h"

int main(int argc, const char *argv[]) {
    App::getInstance()->start(argc, argv, App::PROD);
    return 0;
}
