#ifndef DB_MPDA_APP_H
#define DB_MPDA_APP_H

#include "Constants/Headers/types.h"
#include <string>
#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include "DB/Headers/DbInfo.h"
#include "Exceptions/Headers/EmptyInputCommandLineException.h"
#include "Exceptions/Headers/DatabaseArgumentNotFoundException.h"
#include "Query/Headers/QueryFactory.h"
#include "Exceptions/Headers/TableNotExistsException.h"

class App {
protected:
    App();

    static App *app_;

private:

    void handleCommandLine(int argc, const char *argv[]);

    void launchMainReader();

    void displayUserIndications();

public :
    App(App &other) = delete;

    void operator=(const App &) = delete;

    static App *getInstance();

    void start(int argc, const char **argv, const std::string &env);

    static std::string DEBUG;

    static std::string PROD;

    static void exitOnSuccess();

    static void drawHyphenLine(const int colNumber);

    static int COL_LENGTH;

    static string center(int width, const string &str);

    static void
    drawTable(const string &tableName, const vector<std::string> &headers,
              const vector<std::vector<std::string>> &fields);
};

#endif //DB_MPDA_APP_H
