
#include "Query/Headers/CreateTableQuery.h"

CreateTableQuery::CreateTableQuery(std::string query) : SqlQuery(query) {}

void CreateTableQuery::check() {
    std::smatch m;
    std::regex regex(SQL_REGEX::CREATE_QUERY_ALL);
    if (!std::regex_search(this->getQuery(), m, regex)) {
        throw CheckingSqlQueryException();
    }
}

void CreateTableQuery::parse() {
    std::smatch m;
    std::regex regex(SQL_REGEX::CREATE_QUERY_TABLE);
    if (std::regex_search(this->getQuery(), m, regex)) {
        this->tableName = m[1];
    } else {
        throw ParseSqlQueryException("Table's name not found");
    }
    regex = SQL_REGEX::CREATE_QUERY_ATTRIBUTES;
    if (std::regex_search(this->getQuery(), m, regex)) {
        std::vector<std::string> vectorParams = Library::explodeWithDelimiter(
                Library::removeParentheses(m[0]), ','
        );
        std::vector<std::string> line;
        field_type_t type;
        std::string elementTrimed;
        for (const std::string &elem: vectorParams) {
            elementTrimed = Library::trimString(elem);
            line = Library::explodeWithDelimiter(elementTrimed, ' ');
            type = DefinitionFile::getFieldTypeByName(
                    Library::replaceAllOccurrences(elementTrimed, line.front(), "")
            );
            field f = {type, line.front()};
            this->attributes.push_back((f));
        }
    } else {
        throw ParseSqlQueryException("CHECK YOUR QUERY SYNTAX");
    }
}

void CreateTableQuery::execute() {
    std::string directoryPath = DbInfo::getInstance()->getDbPath().append("/").append(this->tableName).append("/");
    if (Files::knowIfDirectoryExists(directoryPath)) {
        throw TableAlreadyExistsException("Table [" + this->tableName + "] already exists ! ");
    } else {
        Files::createDirectory(directoryPath);
        DefinitionFile definitionFile(tableName);
        definitionFile.writeDefinition(this->attributes);

        KeyFile keyFile(tableName);
        keyFile.generateKeyFile();
        std::cout << "YOUR TABLE " << tableName << " has been created !" << std::endl;
    }
}

void CreateTableQuery::expand() {

}


