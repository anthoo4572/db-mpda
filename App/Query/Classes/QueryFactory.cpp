#include "Query/Headers/QueryFactory.h"


std::unique_ptr<SqlQuery> QueryFactory::create(const std::string &query) {
    const std::string new_query = Library::toLowerCase(Library::trimString(query.substr(0, query.size() - 1)));
    std::vector<std::string> words = Library::explodeWithDelimiter(new_query, ' ');

    if (words.size() < 2) {
        throw UnknownSqlException("QUERY INCORRECT");
    }

    std::string firstWord = Library::trimString(words[0]);
    std::string secondWord = Library::trimString(words[1]);

    if (firstWord == SQL_KEYWORD::CREATE) {
        return std::move(std::make_unique<CreateTableQuery>(new_query));
    } else if (firstWord == SQL_KEYWORD::INSERT) {
        return std::move(std::make_unique<InsertQuery>(new_query));
    } else if (firstWord == SQL_KEYWORD::SELECT) {
        return std::move(std::make_unique<SelectQuery>(new_query));
    } else if (firstWord == SQL_KEYWORD::UPDATE) {
        throw FeatureNotImplementedException();
    } else if (firstWord == SQL_KEYWORD::DELETE) {
        return std::move(std::make_unique<DeleteQuery>(new_query));
    } else if (firstWord == SQL_KEYWORD::DROP && secondWord == SQL_KEYWORD::TABLE) {
        return std::move(std::make_unique<DropTableQuery>(new_query));
    } else if (firstWord == SQL_KEYWORD::DROP &&
               (secondWord == SQL_KEYWORD::DATABASE || secondWord == SQL_KEYWORD::DB)) {
        return std::move(std::make_unique<DropDatabaseQuery>(new_query));
    } else if (firstWord == SQL_KEYWORD::DESCRIBE) {
        return std::move(std::make_unique<DescribeTableQuery>(new_query));
    } else if (firstWord == SQL_KEYWORD::SHOW && secondWord == SQL_KEYWORD::TABLES) {
        return std::move(std::make_unique<ShowTablesQuery>(new_query));
    } else {
        throw UnknownSqlException("QUERY INCORRECT : <" + new_query + ">");
    }
    return nullptr;
}
