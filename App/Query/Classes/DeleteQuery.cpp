#include "Query/Headers/DeleteQuery.h"


DeleteQuery::DeleteQuery(const std::string &query) : SqlQuery(query) {}

void DeleteQuery::check() {
    std::smatch m;
    std::regex regex(SQL_REGEX::DELETE_ALL);
    if (!std::regex_search(this->getQuery(), m, regex)) {
        throw CheckingSqlQueryException();
    }
}

void DeleteQuery::parse() {
    std::smatch m;
    std::regex regex(SQL_REGEX::DELETE_ALL);
    if (std::regex_search(this->getQuery(), m, regex)) {
        this->tableName = Library::trimString(m[3]);
    } else {
        throw ParseSqlQueryException("Table's name not found");
    }
}


void DeleteQuery::execute() {
    IndexFile indexFile(this->tableName);
    index_entry val{};
    for (int i = 1; i < indexFile.getNumberOfLine() + 1; i++) {
        val = indexFile.getIndexEntry(i);
        if (val.is_active) {
            val.is_active = false;
            indexFile.updateIndexEntry(val, i);
        }
    }
    std::cout << "RECORDS HAS BEEN DELETED" << std::endl;
}

void DeleteQuery::expand() {

}
