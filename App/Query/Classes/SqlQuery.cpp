#include "Query/Headers/SqlQuery.h"

SqlQuery::SqlQuery(std::string query) : query(std::move(query)) {}

const std::string &SqlQuery::getQuery() const {
    return query;
}

void SqlQuery::setQuery(const std::string &q) {
    SqlQuery::query = q;
}
