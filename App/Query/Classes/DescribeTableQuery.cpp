#include "Query/Headers/DescribeTableQuery.h"

DescribeTableQuery::DescribeTableQuery(const std::string &query) : SqlQuery(query) {}

void DescribeTableQuery::check() {
    std::smatch m;
    std::regex regex(SQL_REGEX::DESCRIBE_TABLE);
    if (!std::regex_search(this->getQuery(), m, regex)) {
        throw CheckingSqlQueryException();
    }
}

void DescribeTableQuery::parse() {
    std::smatch m;
    std::regex regex(R"(describe ([a-z]+)(\s)*)");
    if (std::regex_search(this->getQuery(), m, regex) && DbInfo::getInstance()->knowIfTableExists(m[1])) {
        this->tableName = m[1];
    } else {
        throw ParseSqlQueryException("This table doesn't exists!");
    }
}


void DescribeTableQuery::expand() {

}

void DescribeTableQuery::execute() {
    DefinitionFile definitionFile(tableName);
    const std::vector<field> fields = definitionFile.readDefinition().fieldsVector;
    std::vector<std::vector<std::string>> res;
    std::vector<std::string> line;
    for (const field &f: fields) {
        line.clear();
        line.push_back(f.fieldName);
        line.push_back(DefinitionFile::getFieldTypeNameByType(f.fieldType));
        res.push_back(line);
    }
    App::drawTable("TABLE : " + tableName, {"name", "type"}, res);
}
