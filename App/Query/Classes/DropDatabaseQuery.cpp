#include "Query/Headers/DropDatabaseQuery.h"

DropDatabaseQuery::DropDatabaseQuery(const std::string &query) : SqlQuery(query) {}

void DropDatabaseQuery::check() {
    std::smatch m;
    std::regex regex(SQL_REGEX::DROP_DATABASE);
    if (!std::regex_search(this->getQuery(), m, regex)) {
        throw CheckingSqlQueryException();
    }
}

void DropDatabaseQuery::parse() {
}


void DropDatabaseQuery::expand() {

}

void DropDatabaseQuery::execute() {
    std::string valueRead;
    bool correctValue = false, drop = false;
    std::cerr << "ARE YOU SURE YOU WANT TO DROP THE DATABASE ? (YES|NO) > ";
    while (!correctValue && std::getline(std::cin, valueRead)) {
        drop = Library::trimString(valueRead) == "YES";
        if (drop || Library::trimString(valueRead) == "NO") {
            correctValue = true;
        } else {
            std::cout << "INCORRECT VALUE > ";
        }
    }
    if (drop) {
        std::error_code errorCode;
        if (!std::filesystem::remove_all(DbInfo::getInstance()->getDbPath(), errorCode)) {
            throw ExecutionSqlQueryException(errorCode.message());
        } else {
            std::cout << "DATABASE has been dropped!" << std::endl;
            App::exitOnSuccess();
        }
    }
}
