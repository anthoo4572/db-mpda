#include "Query/Headers/DropTableQuery.h"


DropTableQuery::DropTableQuery(const std::string &query) : SqlQuery(query) {}


void DropTableQuery::check() {
    std::smatch m;
    std::regex regex(SQL_REGEX::DROP_TABLE
    );
    if (!std::regex_search(this->getQuery(), m, regex)) {
        throw CheckingSqlQueryException();
    }
}

void DropTableQuery::parse() {
    std::smatch m;
    std::regex regex(R"(table ([a-z]+)(\s)*)");
    if (std::regex_search(this->getQuery(), m, regex) && DbInfo::getInstance()->knowIfTableExists(m[1])) {
        this->tableName = m[1];
    } else {
        throw ParseSqlQueryException("This table doesn't exists!");
    }
}

void DropTableQuery::expand() {

}

void DropTableQuery::execute() {
    std::error_code errorCode;
    if (!std::filesystem::remove_all(Files::getTableFolderPath(tableName), errorCode)) {
        throw ExecutionSqlQueryException(errorCode.message());
    } else {
        std::cout << "Table has been dropped!" << std::endl;
    }
}