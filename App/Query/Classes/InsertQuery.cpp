//
// Created by matth on 12/02/2022.
//

#include "Query/Headers/InsertQuery.h"

InsertQuery::InsertQuery(std::string _query) : SqlQuery(std::move(_query)) {

}

void InsertQuery::parse() {
    std::smatch matchsAll;
    std::regex regexpAll(SQL_REGEX::INSERT_QUERRY_ALL);
    std::regex_search(this->getQuery(), matchsAll, regexpAll);

    tableName = (std::string)matchsAll[1];

    //if table exist
    if(DbInfo::getInstance()->knowIfTableExists(tableName)) {

        DefinitionFile defFile(tableName);
        tableDef = defFile.readDefinition();

        pkName = defFile.getColumnNameByType(PRIMARY_KEY)[0];

        //if fields specified
        if (!fieldsList.empty()) {

            //if there is all field in query
            if (fieldsList.size() == tableDef.fieldsVector.size()) {
                isPk = true;
                //iterate field in query
                existAndCastOkWithField();
            }
                //PK may not be give
            else {
                // check if PK was given
                for (const string& field: fieldsList) {
                    if (field == pkName) {
                        isPk = true;
                    }
                }
                if (!isPk && fieldsList.size() == tableDef.fieldsVector.size() - 1) {
                    existAndCastOkWithField();
                } else {
                    throw CheckingSqlQueryException(
                            "Fields doesn't correspond with table. There is " + to_string(fieldsList.size()) +
                            " field(s) in query but " + to_string(tableDef.fieldsVector.size()) + " field(s) in table");
                }
            }
        }
            //field wasn't given
        else {
            if (valuesList.size() == tableDef.fieldsVector.size()){
                isPk=true;
                existAndCastOkWithoutField();
            }
            else if (valuesList.size() == tableDef.fieldsVector.size()-1){
                existAndCastOkWithoutField();
            }
            else{
                throw CheckingSqlQueryException(
                        "Fields doesn't correspond with table. There is " + to_string(valuesList.size()) +
                        " field(s) in query but " + to_string(tableDef.fieldsVector.size()) + " field(s) in table");
            }
        }
    }
    else{
        throw CheckingSqlQueryException("Table does not exist");
    }
}

void InsertQuery::check() {
    std::smatch matchs;
    std::regex regexp(SQL_REGEX::INSERT_QUERRY_ALL);

    if (!std::regex_search(this->getQuery(), matchs, regexp)) {
        throw ParseSqlQueryException("Impossible to parse your query (\"'\" and \",\" are not accepted in string)");
    }

    //get values
    std::string values = (std::string)matchs[5];
    this->valuesList = Library::explodeWithDelimiter(values, ',');
    for (int i = 0; i<valuesList.size(); i++){
        valuesList[i] = Library::replaceAllOccurrences(valuesList[i],"'", "");
    }

    std::regex regexpField(SQL_REGEX::INSERT_QUERRY_FIELD);
    std::smatch matchsField;

    //if fields specified
    if(std::regex_search(this->getQuery(), matchsField, regexpField)){
        //get fields
        std::string fields = (std::string)matchs[3];
        fields = Library::replaceAllOccurrences(fields," ", "");
        this->fieldsList = Library::explodeWithDelimiter(fields, ',');

        if(valuesList.size() != fieldsList.size()){
            throw ParseSqlQueryException("Number of values doesn't correspond with field's number");
        }
    }
}

void InsertQuery::expand() {

}

void InsertQuery::execute() {
    if(!fieldsList.empty()){
        throw FeatureNotImplementedException();
    }
    else{ //fields not provided
        if (isPk){
            for(int i=0; i<tableDef.fieldsVector.size(); i++){
                keyValue tmpKeyValue{tableDef.fieldsVector[i].fieldName, valuesList[i], tableDef.fieldsVector[i].fieldType};

                listKeyValue.push_back(tmpKeyValue);
            }

            ContentFile contentFile(tableName);
            IndexFile indexFile(tableName);
            DefinitionFile defFile(tableName);
            tuple TUPLE = indexFile.getFirstUnactiveIndex();

            //write over unactive index and content
            if(get<1>(TUPLE)){
                index_entry unactiveIndex = get<0>(TUPLE);
                unactiveIndex.is_active = true;
                indexFile.updateIndexEntry(unactiveIndex, get<2>(TUPLE));

                printContent((unactiveIndex.position-1)*unactiveIndex.length);
            }
            else{  //write new index and content
                long long offset = contentFile.getLastOffset();

                //determine position of cursor
                int position = offset/defFile.getContentLength();
                position++;

                int length = defFile.getContentLength();

                //print index
                index_entry newIndex = {true, (uint32_t)position ,(uint16_t)length };
                indexFile.writeIndexEntry(newIndex);

                //print content
                printContent(offset);
            }
            pair<bool, int> maxPk = getMaxPk();

            //update key file
            KeyFile keyFile(tableName);
            keyFile.updateKey(maxPk.second);

        }
        //pk not provided
        else{
            KeyFile keyFile(tableName);

            int j = 0;
            for(auto & elem : tableDef.fieldsVector){
                if(elem.fieldType != PRIMARY_KEY){
                    keyValue tmpKeyValue{elem.fieldName, valuesList[j], elem.fieldType};
                    listKeyValue.push_back(tmpKeyValue);
                    j++;
                }
                else{
                    pkValue = keyFile.getNextKey();
                    keyValue tmpKeyValue{elem.fieldName, to_string(pkValue), elem.fieldType};
                    listKeyValue.push_back(tmpKeyValue);
                }
            }

            ContentFile contentFile(tableName);
            IndexFile indexFile(tableName);
            DefinitionFile defFile(tableName);
            tuple TUPLE = indexFile.getFirstUnactiveIndex();

            //write over unactive index and content
            if(get<1>(TUPLE)){
                index_entry unactiveIndex = get<0>(TUPLE);
                unactiveIndex.is_active = true;
                indexFile.updateIndexEntry(unactiveIndex, get<2>(TUPLE));

                printContent((unactiveIndex.position-1)*unactiveIndex.length);
            }
            else{  //write new index and content
                long long offset = contentFile.getLastOffset();

                //determine position of cursor
                int position = offset/defFile.getContentLength();
                position++;

                int length = defFile.getContentLength();

                //print index
                index_entry newIndex = {true, (uint32_t)position ,(uint16_t)length };
                indexFile.writeIndexEntry(newIndex);

                //print content
                printContent(offset);
            }

            //update key file
            keyFile.updateKey(pkValue);
        }

    }
    cout << "YOUR RECORD WAS BEEN ADDED" << endl;
}

void InsertQuery::existAndCastOkWithField() {
    for(int i = 0; i<fieldsList.size(); i++){
        bool isFind = false;
        //iterate table's field
        for(const auto& tableElem : tableDef.fieldsVector){
            //if name correspond
            if (tableElem.fieldName == fieldsList[i]){
                isFind = true;

                switch(tableElem.fieldType){
                    case PRIMARY_KEY :
                        try{
                            int value = stoi(valuesList[i]);
                            isPkExists(value);
                        }
                        catch(exception &ex){
                            throw CheckingSqlQueryException("Error while parse string to primary key for \""+fieldsList[i]+"\"; value : "+valuesList[i]);
                        }
                        break;
                    case INT :
                        try{
                            stoi(valuesList[i]);
                        }
                        catch(exception &ex){
                            throw CheckingSqlQueryException("Error while parse string to int for \""+fieldsList[i]+"\"; value : "+valuesList[i]);
                        }
                        break;
                    case FLOAT:
                        try{
                            stof(valuesList[i]);
                        }
                        catch(exception &ex){
                            throw CheckingSqlQueryException("Error while parse string to float for \""+fieldsList[i]+"\"; value : "+valuesList[i]);
                        }
                        break;
                    case TEXT:
                        if(valuesList[i].size() > 150){
                            throw CheckingSqlQueryException("Size too long for \""+fieldsList[i]+"\" (max 150 char); value : "+valuesList[i]);
                        }
                        else{
                            valuesList[i] = Library::completeStringToLength(valuesList[i], 150-valuesList[i].size());
                        }
                        break;
                    default:
                        throw CheckingSqlQueryException("Error while casting");
                }
            }
        }
        if(!isFind){
            throw CheckingSqlQueryException("A field doesn't correspond with table : "+fieldsList[i]);
        }
    }
}

void InsertQuery::existAndCastOkWithoutField(){
    ContentFile contentFile(tableName);
    int j=0;
    for(int i = 0; i<tableDef.fieldsVector.size(); i++){
        switch (tableDef.fieldsVector[i].fieldType){
            case PRIMARY_KEY :
                if (isPk){
                    try{
                        int value = stoi(valuesList[j]);
                        isPkExists(value);
                        j++;
                    }
                    catch(exception &ex){
                        throw CheckingSqlQueryException("Error while parse string to primary key for \""+tableDef.fieldsVector[i].fieldName+"\"; value : "+valuesList[j]+"("+ex.what()+")");
                    }
                }
                break;
            case INT :
                try{
                    stoi(valuesList[j]);
                    j++;
                }
                catch(exception &ex){
                    throw CheckingSqlQueryException("Error while parse string to int for \""+tableDef.fieldsVector[i].fieldName+"\"; value : "+valuesList[j]);
                }
                break;
            case FLOAT :
                try{
                    stof(valuesList[j]);
                    j++;
                }
                catch(exception &ex){
                    throw CheckingSqlQueryException("Error while parse string to float for \""+tableDef.fieldsVector[i].fieldName+"\"; value : "+valuesList[j]);
                }
                break;
            case TEXT :
                if(valuesList[j].size() > 150){
                    throw CheckingSqlQueryException("Size too long for \""+tableDef.fieldsVector[i].fieldName+"\" (max 150 char); value : "+valuesList[j]);
                }
                else{
                    valuesList[j] = Library::completeStringToLength(valuesList[j], 150-valuesList[j].size());
                }
                j++;
                break;
            default :
                throw CheckingSqlQueryException("Error while casting");

        }
    }
}

void InsertQuery::isPkExists (int value){
    ContentFile contentFile(tableName);
    vector<vector<colValue>> allContentLine = contentFile.getAllLineActive(tableName);
    for(const vector<colValue>& elem: allContentLine){
        for(const auto& col : elem){
            if (col.fieldName==pkName && stoi(col.value) == value)
                throw CheckingSqlQueryException("Primary key already exists");
        }
    }
}

pair<bool, int> InsertQuery::getMaxPk (){
    ContentFile contentFile(tableName);
    vector<vector<colValue>> allContentLine = contentFile.getAllLineActive(tableName);
    int MaxValue = 0;
    bool thereIsMax = false;
    for(const vector<colValue>& elem: allContentLine){
        for(const auto& col : elem){
            if (col.fieldName==pkName && stoi(col.value) > MaxValue){
                MaxValue = stoi(col.value);
                thereIsMax = true;
            }
        }
    }
    return pair<bool,int>{thereIsMax, MaxValue};
}

void InsertQuery::printContent(int offset){
    ContentFile contentFile(tableName);
    vector<uint8_t> toPrint;
    string tmpStr;
    for(const keyValue& elem : listKeyValue){
        switch(elem.type){
            case PRIMARY_KEY:
            case INT:
                tmpStr = bitset<64>(stoi(elem.value)).to_string();
                toPrint = {tmpStr.begin(), tmpStr.end()};
                contentFile.write_record(toPrint, offset);
                offset+=DefinitionFile::getLengthOfType(INT);
                break;

            case FLOAT:
                tmpStr = FloatConverter::convertFloatIntoBinaryString(stof(elem.value));
                toPrint = {tmpStr.begin(), tmpStr.end()};
                contentFile.write_record(toPrint, offset);
                offset+=DefinitionFile::getLengthOfType(FLOAT);
                break;

            case TEXT:
                tmpStr = elem.value;
                toPrint = {tmpStr.begin(), tmpStr.end()};
                contentFile.write_record(toPrint, offset);
                offset+=DefinitionFile::getLengthOfType(TEXT);
                break;
            default:
                throw ExecutionSqlQueryException("Type unknown while printing");
        }
    }
}