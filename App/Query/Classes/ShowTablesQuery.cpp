#include "Query/Headers/ShowTablesQuery.h"

ShowTablesQuery::ShowTablesQuery(const std::string &query) : SqlQuery(query) {}


void ShowTablesQuery::check() {

}

void ShowTablesQuery::parse() {

}

void ShowTablesQuery::expand() {

}

void ShowTablesQuery::execute() {
    std::vector<std::vector<std::string>> res;
    for (std::string table: DbInfo::getInstance()->getExistingTables()) {
        res.push_back({{table}});
    }
    App::drawTable(this->getQuery() + ";", {"tables"}, res);;
}
