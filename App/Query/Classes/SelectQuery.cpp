#include "Query/Headers/SelectQuery.h"


SelectQuery::SelectQuery(const string &query) : SqlQuery(query) {}

void SelectQuery::check() {
    std::smatch m;
    std::regex regex(SQL_REGEX::SELECT_ALL);
    if (!std::regex_search(this->getQuery(), m, regex)) {
        throw CheckingSqlQueryException();
    }
}

void SelectQuery::parse() {
    std::smatch m;
    std::regex regex(SQL_REGEX::SELECT_TABLE);
    if (std::regex_search(this->getQuery(), m, regex)) {
        this->tableName = m[2];
    } else {
        throw ParseSqlQueryException("Table's name not found");
    }
    if (DbInfo::getInstance()->knowIfTableExists(tableName)) {
        regex = SQL_REGEX::SELECT_ALL_ATTRIBUTES;
        if (std::regex_search(this->getQuery(), m, regex)) { // SELECT *
            expand();
        }
        regex = SQL_REGEX::SELECT_ALL;
        if (std::regex_search(this->getQuery(), m, regex)) {
            for (const std::string &col: Library::explodeWithDelimiter(m[2], ',')) {
                if (!DbInfo::getInstance()->knowIfColumnExistsInTable(tableName, Library::trimString(col))) {
                    this->columnsWanted.clear();
                    throw ColumnNotInTableException(
                            "COLUMN < " + col + " > WAS NOT FOUND IN TABLE < " + tableName + " >");
                }
                this->columnsWanted.push_back(Library::trimString(col));
            }
        }
        regex = SQL_REGEX::SELECT_WHERE;
        if (std::regex_search(this->getQuery(), m, regex)) {
            DefinitionFile definitionFile(this->tableName);
            this->isWithWhere = true;
            std::string allWhere = m[0];
            allWhere = Library::trimString(Library::replaceAllOccurrences(allWhere, "where", ""));
            const int numberOfAnd = Library::getNumberOfValue(allWhere, "and");
            const int numberOfOr = Library::getNumberOfValue(allWhere, "or");
            if ((numberOfAnd != 0 && numberOfOr == 0) | (numberOfOr != 0 && numberOfAnd == 0) ||
                (numberOfOr == 0 && numberOfAnd == 0)) {
                this->isAndWhereQuery = numberOfAnd != 0;
                this->whereValues = DbInfo::handleWhereQuery(allWhere);
                for (const whereOperation &whereOperation: this->whereValues) {
                    if (!DbInfo::getInstance()->knowIfColumnExistsInTable(tableName, whereOperation.colName)) {
                        throw ColumnNotInTableException(
                                "[WHERE] COLUMN < " + whereOperation.colName + " > WAS NOT FOUND IN TABLE < " +
                                tableName + " >");
                    } else {
                        const field_type_t type = definitionFile.getColumnTypeWithColName(whereOperation.colName);
                        DefinitionFile::checkValueTypeWithColType(whereOperation.colName, whereOperation.value, type);
                    }
                }
            } else {
                throw AndOrInWhereClauseException();
            }
        }
        IndexFile indexFile(tableName);
        ContentFile contentFile(tableName);
        DefinitionFile definitionFile(tableName);
        std::vector<field> fields = definitionFile.readDefinition().fieldsVector;
        index_entry val{};
        std::vector<colValue> rowSelected;
        for (int i = 1; i < indexFile.getNumberOfLine() + 1; i++) {
            val = indexFile.getIndexEntry(i);
            if (val.is_active) {
                rowSelected = contentFile.getValuesOfRecord(val.position,
                                                            fields,
                                                            val.length);
                std::vector<bool> equals;
                if (isWithWhere) {
                    for (const colValue &colValue: rowSelected) { // SELECT
                        for (const whereOperation &where: this->whereValues) { // WHERE
                            if (colValue.fieldName == where.colName) {
                                bool valueToPush;
                                switch (where.operatorr) {
                                    case EQUAL:
                                        valueToPush = std::strcmp(colValue.value.c_str(), where.value.c_str()) == 0;
                                        break;
                                    case NOT_EQUAL:
                                        valueToPush = std::strcmp(colValue.value.c_str(), where.value.c_str()) != 0;
                                        break;
                                    case LOWER_THAN:
                                        valueToPush = std::strcmp(colValue.value.c_str(), where.value.c_str()) < 0;
                                        break;
                                    case LOWER_EQUAL_THAN:
                                        valueToPush = std::strcmp(colValue.value.c_str(), where.value.c_str()) <= 0;
                                        break;
                                    case GREATER_THAN:
                                        valueToPush = std::strcmp(colValue.value.c_str(), where.value.c_str()) > 0;
                                        break;
                                    case GREATER_EQUAL_THAN:
                                        valueToPush = std::strcmp(colValue.value.c_str(), where.value.c_str()) >= 0;
                                        break;
                                    default :
                                        throw WhereOperatorNotFoundException("");
                                }
                                equals.push_back(valueToPush);
                            }
                        }
                    }
                    if (this->isAndWhereQuery) {
                        if (!(std::find(equals.begin(), equals.end(), false) != equals.end())) {
                            this->linesSelected.push_back(rowSelected);
                        }
                    } else {
                        if (std::find(equals.begin(), equals.end(), true) != equals.end()) {
                            this->linesSelected.push_back(rowSelected);
                        }
                    }
                } else {
                    this->linesSelected.push_back(rowSelected);
                }
            }
        }
        std::vector<std::string> lines;
        for (const auto &cols: this->linesSelected) {
            lines.clear();
            for (const std::string &colWanted: this->columnsWanted) {
                for (const auto &col: cols) {
                    if (colWanted == col.fieldName) {
                        lines.push_back(col.value);
                    }
                }
            }
            this->valuesToDisplay.push_back(lines);
        }
    } else {
        throw TableNotExistsException("Table " + tableName + " does not exists !");
    }
}


void SelectQuery::execute() {
    App::drawTable(this->originalQuery + ";", this->columnsWanted, this->valuesToDisplay);
    int lineSize = int(this->linesSelected.size());
    std::cout << lineSize << " line" << (lineSize > 1 ? "s" : "") << " returned" << std::endl;
}

void SelectQuery::expand() {
    this->originalQuery = this->getQuery();
    const std::string newQuery = Library::replaceAllOccurrences(
            this->getQuery(), "\\*",
            Library::implodeWithDelimiter(DbInfo::getInstance()->getAllColumnsOfTable(this->tableName), ","));
    this->setQuery(newQuery);
}


whereOperator SelectQuery::getOperatorType(const std::string &operatorr) {
    if (operatorr == "=") {
        return EQUAL;
    } else if (operatorr == "<>") {
        return NOT_EQUAL;
    } else if (operatorr == "<") {
        return LOWER_THAN;
    } else if (operatorr == "<=") {
        return LOWER_EQUAL_THAN;
    } else if (operatorr == ">") {
        return GREATER_THAN;
    } else if (operatorr == ">=") {
        return GREATER_EQUAL_THAN;
    } else {
        throw WhereOperatorNotFoundException(operatorr);
    }
}
