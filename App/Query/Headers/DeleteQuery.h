#ifndef DB_MPDA_DELETEQUERY_H
#define DB_MPDA_DELETEQUERY_H

#include "Constants/Headers/types.h"
#include "SqlQuery.h"
#include <regex>
#include "Constants/Headers/SQL_REGEX.h"
#include "Exceptions/Headers/CheckingSqlQueryException.h"
#include "Exceptions/Headers/ParseSqlQueryException.h"
#include "ManageFiles/Headers/IndexFile.h"

class DeleteQuery : public SqlQuery {
private:
    std::string tableName;
public:
    DeleteQuery(const std::string &query);

    void parse() override;

    void check() override;

    void expand() override;

    void execute() override;
};


#endif //DB_MPDA_DELETEQUERY_H
