//
// Created by matth on 12/02/2022.
//

#ifndef DB_MPDA_INSERTQUERRY_H
#define DB_MPDA_INSERTQUERRY_H

#include "Constants/Headers/types.h"
#include "SqlQuery.h"
#include <vector>
#include <utility>
#include <regex>
#include <iostream>
#include "ManageFiles/Headers/DefinitionFile.h"
#include "Constants/Headers/SQL_REGEX.h"
#include "Exceptions/Headers/ParseSqlQueryException.h"
#include "DB/Headers/DbInfo.h"
#include "ManageFiles/Headers/DefinitionFile.h"
#include "Exceptions/Headers/CheckingSqlQueryException.h"
#include "ManageFiles/Headers/KeyFile.h"
#include "ManageFiles/Headers/IndexFile.h"
#include "Exceptions/Headers/ExecutionSqlQueryException.h"
#include "Utils/Headers/FloatConverter.h"

class InsertQuery : public SqlQuery {
private:
    std::vector<std::string> fieldsList;
    std::vector<std::string> valuesList;
    vector<keyValue> listKeyValue;
    string pkName;
    int pkValue;
    bool isPk = false;
    table_definition tableDef;
    string tableName;

    void existAndCastOkWithField();

    void existAndCastOkWithoutField();

public:
    InsertQuery(std::string _query);

    void parse() override;

    void check() override;

    void expand() override;

    void execute() override;

    void isPkExists(int value);

    pair<bool, int> getMaxPk();

    void printContent(int offset);
};


#endif //DB_MPDA_INSERTQUERRY_H
