#ifndef DB_MPDA_DROPTABLEQUERY_H
#define DB_MPDA_DROPTABLEQUERY_H

#include "Constants/Headers/types.h"
#include "SqlQuery.h"
#include "Utils/Headers/Files.h"
#include "DB/Headers/DbInfo.h"
#include "Exceptions/Headers/CheckingSqlQueryException.h"
#include "Exceptions/Headers/ParseSqlQueryException.h"
#include "Exceptions/Headers/ExecutionSqlQueryException.h"

class DropTableQuery : public SqlQuery {
private :
    std::string tableName;
public:
    DropTableQuery(const std::string &query);

private:
    void parse() override;

    void check() override;

    void expand() override;

    void execute() override;
};


#endif //DB_MPDA_DROPTABLEQUERY_H
