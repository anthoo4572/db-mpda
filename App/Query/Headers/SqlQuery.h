#ifndef DB_MPDA_SQLQUERY_H
#define DB_MPDA_SQLQUERY_H

#include "Constants/Headers/types.h"
#include <string>
#include <utility>

class SqlQuery {

private:
    std::string query;
public:
    explicit SqlQuery(std::string query);

    virtual void parse() = 0;

    virtual void check() = 0;

    virtual void expand() = 0;

    virtual void execute() = 0;

    const std::string &getQuery() const;

    void setQuery(const std::string &q);
};


#endif //DB_MPDA_SQLQUERY_H
