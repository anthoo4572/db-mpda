#ifndef DB_MPDA_CREATETABLEQUERY_H
#define DB_MPDA_CREATETABLEQUERY_H

#include "Constants/Headers/types.h"
#include "SqlQuery.h"
#include "ManageFiles/Headers/DefinitionFile.h"
#include "Exceptions/Headers/CheckingSqlQueryException.h"
#include "Exceptions/Headers/ParseSqlQueryException.h"
#include "DB/Headers/DbInfo.h"
#include "Exceptions/Headers/TableAlreadyExistsException.h"
#include "ManageFiles/Headers/KeyFile.h"
#include "Constants/Headers/SQL_REGEX.h"

class CreateTableQuery : public SqlQuery {
private:
    std::string tableName;
    std::vector<field> attributes;
public:

    CreateTableQuery(std::string query);

    void parse() override;

    void check() override;

    void expand() override;

    void execute() override;
};


#endif //DB_MPDA_CREATETABLEQUERY_H
