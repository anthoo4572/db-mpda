#ifndef DB_MPDA_DROPDATABASEQUERY_H
#define DB_MPDA_DROPDATABASEQUERY_H

#include "Constants/Headers/types.h"
#include "SqlQuery.h"
#include "Constants/Headers/SQL_REGEX.h"
#include "Exceptions/Headers/CheckingSqlQueryException.h"
#include "DB/Headers/DbInfo.h"
#include "Exceptions/Headers/ExecutionSqlQueryException.h"
#include "App.h"
#include <regex>

class DropDatabaseQuery : public SqlQuery {
private:
    std::string tableName;
public:
    DropDatabaseQuery(const std::string &query);

    void parse() override;

    void check() override;

    void expand() override;

    void execute() override;

};


#endif //DB_MPDA_DROPDATABASEQUERY_H
