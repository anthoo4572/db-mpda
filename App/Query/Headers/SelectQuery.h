#ifndef DB_MPDA_SELECTQUERY_H
#define DB_MPDA_SELECTQUERY_H

#include "Constants/Headers/types.h"
#include "SqlQuery.h"
#include "ManageFiles/Headers/DefinitionFile.h"
#include "Constants/Headers/SQL_REGEX.h"
#include "Exceptions/Headers/CheckingSqlQueryException.h"
#include "Exceptions/Headers/ParseSqlQueryException.h"
#include "DB/Headers/DbInfo.h"
#include "Exceptions/Headers/ColumnNotInTableException.h"
#include "ManageFiles/Headers/IndexFile.h"
#include "Exceptions/Headers/TableNotExistsException.h"
#include "Exceptions/Headers/AndOrInWhereClauseException.h"
#include "Exceptions/Headers/WhereOperatorNotFoundException.h"
#include "App.h"

class SelectQuery : public SqlQuery {
private:
    std::string originalQuery;
    std::string tableName;
    std::vector<std::string> columnsWanted;
    std::vector<std::vector<colValue>> linesSelected;

    bool isWithWhere = false;
    std::vector<whereOperation> whereValues;
    bool isAndWhereQuery;
    std::vector<std::vector<std::string>> valuesToDisplay;
public:
    SelectQuery(const string &query);

    void parse() override;

    void check() override;

    void expand() override;

    void execute() override;

    static whereOperator getOperatorType(const std::string &operatorr);

};

#endif //DB_MPDA_SELECTQUERY_H
