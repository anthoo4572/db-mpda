#ifndef DB_MPDA_QUERYFACTORY_H
#define DB_MPDA_QUERYFACTORY_H

#include "Constants/Headers/types.h"
#include "SqlQuery.h"
#include "Exceptions/Headers/UnknownSqlException.h"
#include "Query/Headers/CreateTableQuery.h"
#include "Query/Headers/DropTableQuery.h"
#include "Query/Headers/SelectQuery.h"
#include "Utils/Headers/Library.h"
#include "Query/Headers/DeleteQuery.h"
#include "Query/Headers/DropTableQuery.h"
#include "Query/Headers/InsertQuery.h"
#include "Query/Headers/DropDatabaseQuery.h"
#include "Constants/Headers/SQL_KEYWORD.h"
#include "Query/Headers/DescribeTableQuery.h"
#include "Query/Headers/ShowTablesQuery.h"
#include "Exceptions/Headers/FeatureNotImplementedException.h"

class QueryFactory {
public:
    QueryFactory() = delete;

    static std::unique_ptr<SqlQuery> create(const std::string &query);
};


#endif //DB_MPDA_QUERYFACTORY_H
