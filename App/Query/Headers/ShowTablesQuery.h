#ifndef DB_MPDA_SHOWTABLESQUERY_H
#define DB_MPDA_SHOWTABLESQUERY_H

#include "Constants/Headers/types.h"
#include "SqlQuery.h"
#include <iostream>
#include "DB/Headers/DbInfo.h"

class ShowTablesQuery : public SqlQuery {
public:
    ShowTablesQuery(const std::string &query);

    void parse() override;

    void check() override;

    void expand() override;

    void execute() override;
};


#endif //DB_MPDA_SHOWTABLESQUERY_H
