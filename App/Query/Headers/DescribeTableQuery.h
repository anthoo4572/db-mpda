#ifndef DB_MPDA_DESCRIBETABLEQUERY_H
#define DB_MPDA_DESCRIBETABLEQUERY_H

#include "Constants/Headers/types.h"
#include "SqlQuery.h"
#include "Query/Headers/DescribeTableQuery.h"
#include "Constants/Headers/SQL_REGEX.h"
#include "Exceptions/Headers/CheckingSqlQueryException.h"
#include "DB/Headers/DbInfo.h"
#include "App.h"
#include <regex>

class DescribeTableQuery : public SqlQuery {
private :
    std::string tableName;
public:
    DescribeTableQuery(const std::string &query);

    void parse() override;

    void check() override;

    void expand() override;

    void execute() override;

};


#endif //DB_MPDA_DESCRIBETABLEQUERY_H
