#include "App.h"
#include "Exceptions/Headers/TableNotExistsException.h"

std::string App::DEBUG = "DEBUG";

std::string App::PROD = "PROD";

App *App::app_ = nullptr;

App::App() = default;

int App::COL_LENGTH = 20;

App *App::getInstance() {
    if (app_ == nullptr) {
        app_ = new App();
    }
    return app_;
}

void App::start(int argc, const char *argv[], const std::string &env) {
    if (env == App::DEBUG) {

    } else {
        handleCommandLine(argc, argv);
        DbInfo::getInstance("testDB")->checkIfFolderExists();
        launchMainReader();
    }
}

void App::handleCommandLine(int argc, const char *argv[]) {
    char m_databasePath[256];
    getcwd(m_databasePath, 256);
    std::string database_name, database_path;
    for (int i = 0; i < argc; ++i) {
        const std::string arg = argv[i];
        if (arg == "-d") {
            if (i + 1 < argc) {
                database_name = argv[i + 1];
            } else {
                throw EmptyInputCommandLineException("YOU MUST DEFINE A DATABASE");
            }
        }
        if (arg == "-l" && (i + 1) < argc) {
            if (i + 1 < argc) {
                database_path = argv[i + 1];
            } else {
                throw EmptyInputCommandLineException("YOU MUST DEFINE A PATH");
            }
        }
    }
    if (database_name.empty()) {
        throw DatabaseArgumentNotFoundException("YOU MUST NAME THE DATABASE");
    }
    if (database_path.empty()) {
        database_path = m_databasePath;
    }
    DbInfo::getInstance(database_name, database_path + "\\" + database_name);
    std::cout << "PROGRAM LAUNCHED WITH DATABASE : " << "\n\t" << DbInfo::getInstance()->getDbPath() << std::endl;
}

void App::displayUserIndications() {
    const std::vector<std::string> commands = {"Available commands are : ",
                                               "\t CREATE TABLE <name>(<name> <type>);",
                                               "\t INSERT INTO <table> VALUES (<value>);",
                                               "\t SELECT *|attr FROM <name> WHERE <attr> = <value> AND|OR ;",
                                               "\t DELETE FROM TABLE <table>;",
                                               "\t UPDATE TABLE <table> SET <attr> = <value> WHERE <attr> = <value>;",
                                               "\t DROP TABLE <table>;",
                                               "\t DROP DATABASE;",
                                               "\t DROP DB;",
                                               "\t SHOW TABLES;",
                                               "\t DESCRIBE <table>;",
                                               "\t exit();"
    };
    for (const std::string &command: commands) {
        std::cout << command << std::endl;
    }
    std::cout << std::endl;
}

void App::launchMainReader() {
    std::cout << std::endl;
    displayUserIndications();
    std::cout << "READY TO INTERACT WITH DATABASE ! " << std::endl;
    std::string query;
    std::string valueRead;
    while (true) {
        valueRead.clear();
        query.clear();
        std::cout << DbInfo::getInstance()->getDatabaseName() << " > ";
        while (std::getline(std::cin, valueRead) && valueRead.back() != ';') {
            query.append(" ").append(valueRead);
            std::cout << "> ";
        }
        if (valueRead == "exit" || valueRead == "exit;" || valueRead == "exit();") {
            exitOnSuccess();
        } else if (valueRead == "clear" || valueRead == "clear;" || valueRead == "clear();") {
            query.clear();
            std::cout << "query has been clear" << std::endl;
        } else {
            query.append(" ").append(valueRead);
            query = Library::trimString(query);
            try {
                unique_ptr<SqlQuery> sq = QueryFactory::create(query);
                sq->check();
                sq->parse();
                sq->execute();
            }
            catch (BaseException &e) {
                std::cerr << e.what() << endl;
            }
        }
    }
}

void App::exitOnSuccess() {
    std::cout << "GOOD BYE !" << endl;
    exit(EXIT_SUCCESS);
}

void App::drawHyphenLine(const int colNumber) {
    for (int i = 0; i < App::COL_LENGTH * colNumber; ++i) {
        if (i % (App::COL_LENGTH)) {
            std::cout << "-";
        } else {
            std::cout << "|";
        }
    }
    std::cout << "|" << std::endl;
}

void App::drawTable(const std::string &tableName, const std::vector<std::string> &headers,
                    const std::vector<std::vector<std::string>> &fields) {
    std::cout << std::left;
    std::cout << std::endl;
    const int colNumber = int(headers.size());
    App::drawHyphenLine(colNumber);
    std::cout << "|" << center((App::COL_LENGTH - 1) * colNumber, tableName) << "|" << std::endl;
    App::drawHyphenLine(colNumber);
    for (const std::string &col: headers) {
        std::cout << "|" << center(App::COL_LENGTH - 1, col);
    }
    std::cout << "|" << std::endl;
    App::drawHyphenLine(colNumber);
    for (const std::vector<std::string> &line: fields) {
        for (const std::string &col: line) {
            std::cout << "|" << std::setw(App::COL_LENGTH - 1) << Library::trimString(col);
        }
        std::cout << "|" << std::endl;
    }
    App::drawHyphenLine(colNumber);
}

std::string App::center(int width, const string &str) {
    int len = str.length();
    if (width < len) { return str; }
    int diff = width - len;
    int pad1 = diff / 2;
    int pad2 = diff - pad1 + (width % 2 == 0 ? 1 : 0);
    return string(pad1, ' ') + str + string(pad2, ' ');
}