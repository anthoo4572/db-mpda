#include "DB/Headers/DbInfo.h"

DbInfo *DbInfo::dbInfo_ = nullptr;

DbInfo::DbInfo(std::string dbName, std::string dbPath) : databaseName(std::move(dbName)),
                                                         currentDbPath(std::move(dbPath)) {
}

DbInfo *DbInfo::getInstance(std::string dbName, std::string dbPath) {
    if (dbInfo_ == nullptr) {
        dbInfo_ = new DbInfo(std::move(dbName), std::move(dbPath));
    }
    return dbInfo_;
}

void DbInfo::setDbPath(const std::string &path) {
    currentDbPath = path;
}

std::string DbInfo::getDbPath() {
    return currentDbPath;
}

void DbInfo::checkIfFolderExists() {
    if (!(std::filesystem::exists(currentDbPath) && std::filesystem::is_directory(currentDbPath))) {
        std::cout << "CREATING DIRECTORY" << std::endl;
        Files::createDirectory(currentDbPath);
    }
}

const string &DbInfo::getDatabaseName() const {
    return databaseName;
}

std::vector<std::string> DbInfo::getExistingTables() {
    std::vector<std::string> res;
    for (const std::filesystem::directory_entry &dir: std::filesystem::directory_iterator(this->getDbPath())) {
        res.push_back(Library::explodeWithDelimiter(dir.path().string(), '\\').back());
    }
    return res;
}

bool DbInfo::knowIfTableExists(const std::string &tableName) {
    std::vector<std::string> res = DbInfo::getExistingTables();
    return std::find(res.begin(), res.end(), tableName) != res.end();
}

std::vector<std::string> DbInfo::getAllColumnsOfTable(const string &tableName) {
    DefinitionFile definitionFile(tableName);
    std::vector<string> columns;
    for (const field &f: definitionFile.readDefinition().fieldsVector) {
        columns.push_back(f.fieldName);
    }
    return columns;
}

bool DbInfo::knowIfColumnExistsInTable(const string &tableName, const string &columnName) {
    std::vector<std::string> columns = this->getAllColumnsOfTable(tableName);
    return (std::find(columns.begin(), columns.end(), columnName) != columns.end());
}

std::vector<whereOperation> DbInfo::handleWhereQuery(const std::string &query) {
    std::vector<whereOperation> res;
    std::smatch mAll, mPart;
    std::regex regexAll(SQL_REGEX::WHERE_ALL_PARTS), regexPart(SQL_REGEX::WHERE_PART);
    std::string subject = query, part, colName, operatorr, value;
    while (std::regex_search(subject, mAll, regexAll)) {
        part = mAll[0];
        if (std::regex_search(part, mPart, regexPart)) {
            colName = Library::trimString(mPart[1]);
            operatorr = Library::trimString(mPart[2]);
            value = Library::replaceAllOccurrences(mPart[3], "'", "");
            res.push_back(
                    whereOperation({colName, SelectQuery::getOperatorType(operatorr), value})
            );
        }
        subject = mAll.suffix().str();
    }
    return res;
}
