
#ifndef DB_MPDA_DBINFO_H
#define DB_MPDA_DBINFO_H

#include "Constants/Headers/types.h"
#include "Utils/Headers/Library.h"
#include "string"
#include "ManageFiles/Headers/ContentFile.h"
#include "ManageFiles/Headers/DefinitionFile.h"
#include "Constants/Headers/SQL_REGEX.h"
#include "Query/Headers/SelectQuery.h"

struct whereOperation;

class DbInfo {
protected:
    DbInfo(std::string dbName, std::string dbPath);

    static DbInfo *dbInfo_;

private :
    std::string currentDbPath;
    std::string databaseName;

public:
    DbInfo(DbInfo &other) = delete;

    void operator=(const DbInfo &) = delete;

    static DbInfo *getInstance(std::string dbName = "", std::string dbPath = "");

    void setDbPath(const std::string &);

    std::string getDbPath();

    void checkIfFolderExists();

    const string &getDatabaseName() const;

    std::vector<std::string> getExistingTables();

    bool knowIfTableExists(const std::string &tableName);

    std::vector<std::string> getAllColumnsOfTable(const std::string &tableName);

    bool knowIfColumnExistsInTable(const std::string &tableName, const string &columnName);

    static vector<whereOperation> handleWhereQuery(const std::string &query);
};


#endif //DB_MPDA_DBINFO_H
