#include "Utils/Headers/FloatConverter.h"


std::string FloatConverter::convertFloatIntoBinaryString(const float f) {
    floatRepresentation floatRpz;
    floatRpz.f = f;
    std::string res;
    res.append(std::to_string(floatRpz.raw.sign)).append(getStringBinary(floatRpz.raw.exponent, 8)).append(
            getStringBinary(floatRpz.raw.mantissa, 23));
    return res;
}

float FloatConverter::convertBinaryStringIntoFloat(const std::string &str) {
    floatRepresentation floatRpz;
    if (str.length() != 32) {
        throw LengthOfStringNotCorrectException(
                "STRING HAS NOT CORRECT LENGHT : GIVEN " + std::to_string(str.length()) + " | correct : " +
                std::to_string(32)
        );
    }
    unsigned int ieee[32] = {};
    for (int i = 0; i < str.length(); ++i) {
        ieee[i] = str[i] == '0' ? 0 : 1;
    }
    unsigned int f = convertToInt(ieee, 9, 31);
    floatRpz.raw.mantissa = f;
    f = convertToInt(ieee, 1, 8);
    floatRpz.raw.exponent = f;
    floatRpz.raw.sign = ieee[0];
    return floatRpz.f;
}

std::string FloatConverter::getStringBinary(int n, int i) {
    std::string res;
    int k;
    for (k = i - 1; k >= 0; k--) {
        res.append(((n >> k) & 1) ? "1" : "0");
    }
    return res;
}

unsigned int FloatConverter::convertToInt(const unsigned int *arr, int low, int high) {
    unsigned int f = 0, i;
    for (i = high; i >= low; i--) {
        f = f + arr[i] * pow(2, high - i);
    }
    return f;
}