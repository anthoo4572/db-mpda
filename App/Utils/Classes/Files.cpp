#include "Utils/Headers/Files.h"

void Files::createDirectory(const std::string &path) {
    std::filesystem::create_directory(path);
}

bool Files::knowIfDirectoryExists(const std::string &path) {
    return std::filesystem::exists(path);
}

std::string Files::getTableFolderPath(const std::string &tableName) {
    return DbInfo::getInstance()->getDbPath().append("/").append(tableName);
}

std::string Files::generateDefinitionFilePath(const string &tableName) {
    return Files::getTableFolderPath(tableName).append("/").append(tableName).append(".def");
}

std::string Files::generateIndexFilePath(const string &tableName) {
    return Files::getTableFolderPath(tableName).append("/").append(tableName).append(".idx");
}

std::string Files::generateContentFilePath(const string &tableName) {
    return Files::getTableFolderPath(tableName).append("/").append(tableName).append(".data");
}

std::string Files::generateKeyFilePath(const string &tableName) {
    return Files::getTableFolderPath(tableName).append("/").append(tableName).append(".key");
}
