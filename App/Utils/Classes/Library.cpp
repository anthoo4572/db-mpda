
#include "Utils/Headers/Library.h"

std::string Library::toLowerCase(std::string s) {
    std::for_each(s.begin(), s.end(), [](char &c) {
        c = ::tolower(c);
    });
    return s;
}

std::vector<std::string> Library::explodeWithDelimiter(const std::string &s, char delimiter) {
    std::istringstream buffer(s);

    std::string temp;
    std::vector<std::string> res;

    while (std::getline(buffer, temp, delimiter)) {
        res.push_back(temp);
    }
    return res;
}

std::string Library::ltrim(const std::string &s) {
    return std::regex_replace(s, std::regex("^\\s+"), std::string(""));
}

std::string Library::rtrim(const std::string &s) {
    return std::regex_replace(s, std::regex("\\s+$"), std::string(""));
}

std::string Library::trimString(const std::string &s) {
    return Library::ltrim(Library::rtrim(s));
}

std::string
Library::replaceAllOccurrences(const std::string &str, const std::string &oldChar, const std::string &newChar) {
    return Library::trimString(regex_replace(str, std::regex(oldChar), newChar));
}

std::string Library::removeParentheses(const std::string &str) {
    std::string res = Library::replaceAllOccurrences(str, R"(\()", "");
    return Library::replaceAllOccurrences(res, R"(\))", "");
}

std::string
Library::implodeWithDelimiter(const std::vector<std::string> &vec, const std::string &delimiter, int begin,
                              int positionFromEnd) {
    std::string res;
    bool isFirst = true;
    for (auto p = vec.begin() + begin; p != vec.end() - positionFromEnd; ++p) {
        !isFirst ? res.append(delimiter) : "";
        res.append(*p);
        isFirst = false;
    }
    return Library::trimString(res);
}

std::string Library::implodeVectorUINT(const std::vector<uint8_t> &vector) {
    std::string res(vector.begin(), vector.end());
    return res;
}

std::string Library::completeStringToLength(const std::string &col, int val) {
    std::string res = col;
    for (int i = int(res.length()); i < col.length() + val; i++) {
        res.append(" ");
    }
    return res;
}

int Library::getNumberOfValue(const std::string &query, const std::string &value) {
    std::smatch m;
    std::regex regex(value);
    if (std::regex_search(query, m, regex)) {
        return int(m.size());
    } else {
        return 0;
    }
}
