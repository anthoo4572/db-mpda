#ifndef DB_MPDA_FILES_H
#define DB_MPDA_FILES_H

#include "Constants/Headers/types.h"
#include <string>
#include "DB/Headers/DbInfo.h"

class Files {
public:
    Files() = delete;

    ~Files() = delete;

    static void createDirectory(const std::string &path);

    static bool knowIfDirectoryExists(const std::string &path);

    static std::string getTableFolderPath(const std::string &tableName);

    static std::string generateDefinitionFilePath(const std::string &tableName);

    static std::string generateIndexFilePath(const std::string &tableName);

    static std::string generateContentFilePath(const std::string &tableName);

    static string generateKeyFilePath(const string &tableName);
};


#endif //DB_MPDA_FILES_H
