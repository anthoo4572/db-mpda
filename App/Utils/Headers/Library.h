#ifndef DB_MPDA_LIBRARY_H
#define DB_MPDA_LIBRARY_H

#include "Constants/Headers/types.h"
#include <algorithm>
#include <iostream>
#include <sstream>
#include <regex>
#include <filesystem>
#include "Constants/Headers/SQL_REGEX.h"

class Library {

public:
    Library() = delete;

    static std::string toLowerCase(std::string s);

    static std::vector<std::string> explodeWithDelimiter(const std::string &s, char delimiter);

    static std::string rtrim(const std::string &s);

    static std::string ltrim(const std::string &s);

    static std::string trimString(const std::string &s);

    static std::string
    replaceAllOccurrences(const std::string &str, const std::string &oldChar, const std::string &newChar);

    static std::string removeParentheses(const std::string &str);

    static std::string
    implodeWithDelimiter(const std::vector<std::string> &vec, const std::string &delimiter, int begin = 0, int end = 0);

    static std::string implodeVectorUINT(const std::vector<uint8_t> &vector);

    static std::string completeStringToLength(const std::string &col, int val = 15);

    static int getNumberOfValue(const std::string &query, const std::string &value);
};


#endif //DB_MPDA_LIBRARY_H
