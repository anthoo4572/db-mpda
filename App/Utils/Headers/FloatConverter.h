#ifndef DB_MPDA_FLOATCONVERTER_H
#define DB_MPDA_FLOATCONVERTER_H

#include "Constants/Headers/types.h"
#include <string>
#include "Exceptions/Headers/LengthOfStringNotCorrectException.h"
#include <cmath>

class FloatConverter {
public:
    FloatConverter() = delete;

    ~FloatConverter() = delete;

    static std::string convertFloatIntoBinaryString(float f);

    static float convertBinaryStringIntoFloat(const std::string &str);

    static std::string getStringBinary(int n, int i);

    static unsigned int convertToInt(const unsigned int *arr, int low, int high);
};


#endif //DB_MPDA_FLOATCONVERTER_H
