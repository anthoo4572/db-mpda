//
//
// Created by matth on 29/01/2022.
#include <utility>

#include "ManageFiles/Headers/IndexFile.h"

IndexFile::IndexFile(const string &name) : TableFile(Files::generateIndexFilePath(name)) {}

int IndexFile::LINE_LENGTH = 56;

void IndexFile::writeIndexEntry(index_entry &entry) {

    //check existing and open
    this->open();
    this->getFile().seekp(0, ios::end);
    this->getFile() << bitset<8>(entry.is_active) << bitset<32>(entry.position) << bitset<16>(entry.length);
    this->close();
}

void IndexFile::updateIndexEntry(index_entry &entry, uint32_t position) {
    //determine index's position to update
    uint32_t offset = (position - 1) * (IndexFile::LINE_LENGTH);

    //check existing and open
    this->open();
    this->getFile().seekp(offset, ios::beg);
    this->getFile() << bitset<8>(entry.is_active) << bitset<32>(entry.position) << bitset<16>(entry.length);
    this->close();
}

index_entry IndexFile::getIndexEntry(uint32_t position) {
    index_entry index{};

    //determine index's position
    uint32_t offset = (position - 1) * IndexFile::LINE_LENGTH;
    char buffer[56] = {};

    //read file
    if (!this->getFile().is_open()) {
        this->open();
    }
    //set cursor's position
    this->getFile().seekg(offset, ios::beg);
    this->getFile().read(buffer, IndexFile::LINE_LENGTH);
    this->close();

    //truncate string
    string s = (string) buffer;
    string isActive = s.substr(0, 8);
    string posi = s.substr(8, 32);
    string length = s.substr(40, 16);


    //traduct binary to int
    index.is_active = stoi(isActive, nullptr, 2);
    index.position = stoi(posi, nullptr, 2);
    index.length = stoi(length, nullptr, 2);

    return index;
}

int IndexFile::getNumberOfLine() {
    if (!this->getFile().is_open()) {
        this->open();
    }
    this->getFile().seekp(0, ios::end);
    const int number = int(this->getFile().tellp() / IndexFile::LINE_LENGTH);
    this->close();
    return number;
}

tuple<index_entry, bool, int> IndexFile::getFirstUnactiveIndex() {
    index_entry iEntry={};

    int nbLine = getNumberOfLine();

    for(int i = 1; i<=nbLine; i++){
        iEntry = this->getIndexEntry(i);
        if(!iEntry.is_active){
            tuple<index_entry, bool, int> TUPLE(iEntry, true, i);
            return TUPLE;
        }
    }

    tuple<index_entry, bool, int> TUPLE(iEntry, false, 0);

    return TUPLE;
}

