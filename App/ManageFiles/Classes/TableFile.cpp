//
// Created by matth on 22/01/2022.
//

#include "ManageFiles/Headers/TableFile.h"

#pragma region Constructor

TableFile::TableFile() = default;

TableFile::TableFile(string p) : sourcePath(std::move(p)) {}

#pragma endregion

#pragma region Methode

bool TableFile::exists() {
    return std::filesystem::exists(sourcePath);
}

void TableFile::open() {
    if (!this->exists()) {
        this->file.open(this->sourcePath, ios::in | ios::out | ios::binary | ios::app);
        this->file.close();
    }
    this->file.open(this->sourcePath, ios::in | ios::out | ios::binary);
}

void TableFile::close() {
    this->file.close();
}

string TableFile::getSourcePath() {
    return sourcePath;
}

fstream &TableFile::getFile() {
    return file;
}

#pragma endregion
