#include "ManageFiles/Headers/ContentFile.h"

ContentFile::ContentFile(const string &p) : TableFile(Files::generateContentFilePath(p)) {}

void ContentFile::write_record(const vector<uint8_t> &record, uint32_t offset) {
    if (!this->getFile().is_open()) {
        this->open();
    }
    this->getFile().seekp(offset, ios::beg);
    uint32_t cursor = 1;
    for (uint8_t rec: record) {
        this->getFile() << rec;
        this->getFile().seekp(offset + cursor, ios::beg);
        cursor++;
    }
    this->close();
}

std::vector<uint8_t> ContentFile::read_record(uint16_t length, uint32_t offset) {
    if (!this->getFile().is_open()) {
        this->open();
    }
    std::vector<uint8_t> res(std::istreambuf_iterator<char>(this->getFile()), {});
    this->close();
    return {res.begin() + offset, res.begin() + offset + length};
}

vector<colValue>
ContentFile::getValuesOfRecord(const uint32_t linePosition, const vector<field> &fields, const uint16_t lineLength) {
    vector<colValue> res;
    std::vector<uint8_t> vec;
    uint32_t newOffset = (linePosition - 1) * lineLength;
    for (const field &f: fields) {
        vec = ContentFile::read_record(f.length, newOffset);
        std::string newString = Library::implodeVectorUINT(vec);
        std::string val = DefinitionFile::getValueWithType(f.fieldType, newString);
        res.push_back(colValue({f.fieldName, val}));
        newOffset += f.length;
    }
    return res;
}

vector<vector<colValue>> ContentFile::getAllLineActive(const string& tableName) {
    IndexFile indexFile(tableName);
    ContentFile contentFile(tableName);
    DefinitionFile definitionFile(tableName);
    vector<field> fields = definitionFile.readDefinition().fieldsVector;
    vector<colValue> listcolValue;
    vector<vector<colValue>> res;
    index_entry val{};
    for (int i = 1; i <= indexFile.getNumberOfLine(); i++) {
        val = indexFile.getIndexEntry(i);
        if (val.is_active) {
            res.push_back(
                    contentFile.getValuesOfRecord(val.position, fields,
                                                  val.length));
        }
    }
    return res;
}

int ContentFile::getNumberOfLine(const string& tableName) {
    DefinitionFile defFile(tableName);
    int lengthFile = defFile.getContentLength();
    lengthFile++;
    if (!this->getFile().is_open()) {
        this->open();
    }
    this->getFile().seekp(0, ios::end);
    int position = this->getFile().tellp();
    const int number = int(position / lengthFile);
    this->close();
    return number;
}

void ContentFile::writeEndLineChar(const uint32_t offset) {
    if (!this->getFile().is_open()) {
        this->open();
    }
    this->getFile().seekp(offset, ios::beg);
    this->getFile() << std::endl;
    this->close();
}

long long ContentFile::getLastOffset(){
    if (!this->getFile().is_open()) {
        this->open();
    }
    this->getFile().seekp(0, ios::end);
    long long offset = this->getFile().tellp();
    this->close();
    return offset;
}
