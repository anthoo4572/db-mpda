#include "ManageFiles/Headers/DefinitionFile.h"
#include "Utils/Headers/FloatConverter.h"

int DefinitionFile::TEXT_LENGTH = 150;

DefinitionFile::DefinitionFile(const std::string &name) : TableFile(Files::generateDefinitionFilePath(name)) {}

field_type_t DefinitionFile::getFieldTypeById(int number) {
    switch (number) {
        case 0 :
            return PRIMARY_KEY;
        case 1:
            return INT;
        case 2 :
            return TEXT;
        case 3 :
            return FLOAT;
        default:
            throw FieldTypeNotFoundException(&"Field type not found for the number "[number]);
    }
}

int DefinitionFile::getFieldIdByName(const std::string &type) {
    if (type == "primary key") {
        return 0;
    } else if (type == "int") {
        return 1;
    } else if (type == "text") {
        return 2;
    } else if (type == "float") {
        return 3;
    } else {
        throw FieldTypeNotFoundException("Field type not found for the type " + type);
    }
}

field_type_t DefinitionFile::getFieldTypeByName(const std::string &type) {
    return DefinitionFile::getFieldTypeById(
            DefinitionFile::getFieldIdByName(type)
    );
}

table_definition DefinitionFile::get_table_definition() {
    return {};
}

void DefinitionFile::set_table_definition(const table_definition &def) {

}

void DefinitionFile::writeDefinition(const vector<field> &fields) {
    if (!this->getFile().is_open()) {
        this->open();
    }
    for (const field &f: fields) {
        this->getFile() << std::bitset<8>(f.fieldType).to_string() << " " << f.fieldName << "\n";
    }
    this->close();
}

table_definition DefinitionFile::readDefinition() {
    if (!this->getFile().is_open()) {
        this->open();
    }
    vector<field> res;
    std::string line;
    std::vector<std::string> content;
    while (getline(this->getFile(), line)) {
        content = Library::explodeWithDelimiter(line, ' ');
        field_type_t type = DefinitionFile::getFieldTypeById(std::stoi(content[0], nullptr, 2));
        res.push_back((field({type, content[1], DefinitionFile::getLengthOfType(type)})));
    }
    this->close();
    return table_definition({res});
}

int DefinitionFile::getLengthOfType(const field_type_t type) {
    switch (type) {
        case PRIMARY_KEY:
        case INT:
            return 64;
        case FLOAT:
            return 32;
        case TEXT:
            return 150;
    }
    throw FieldTypeNotFoundException(&"Field type not found for the type "[type]);
}

int DefinitionFile::getContentLength() {
    int length = 0;
    for (const field &elem: DefinitionFile::readDefinition().fieldsVector) {
        length += this->getLengthOfType(elem.fieldType);
    }
    return length;
}

string DefinitionFile::getValueWithType(field_type_t type, const std::string &val) {
    switch (type) {
        case 0:
        case 1:
            return to_string(std::stoi(val, nullptr, 2));
        case 3 :
            return to_string(FloatConverter::convertBinaryStringIntoFloat(val));
        case 2:
            return val;
    }
    throw FieldTypeNotFoundException(&"Field type not found for the type "[type]);
}

vector<string> DefinitionFile::getColumnNameByType(field_type_t type) {
    vector<string> response;

    for (const auto &elem: this->readDefinition().fieldsVector) {
        if (elem.fieldType == type) {
            response.push_back(elem.fieldName);
        }
    }

    return response;
}

bool
DefinitionFile::checkValueTypeWithColType(const std::string &colName, const std::string &value,
                                          const field_type_t type) {
    std::string typeWanted;
    switch (type) {
        case PRIMARY_KEY :
            try {
                stoi(value);
            }
            catch (exception &ex) {
                typeWanted = "primary key";
            }
            break;
        case INT :
            try {
                stoi(value);
            }
            catch (exception &ex) {
                typeWanted = "int";
            }
            break;
        case FLOAT:
            try {
                stof(value);
            }
            catch (exception &ex) {
                typeWanted = "float";
            }
            break;
        case TEXT:
            if (value.size() > DefinitionFile::TEXT_LENGTH) {
                throw CheckingSqlQueryException(
                        "YOUR STRING IS TOO LONG (max " + std::to_string(DefinitionFile::TEXT_LENGTH) + ")");
            }
    }
    if (!typeWanted.empty()) {
        throw CheckingSqlQueryException(
                "Error while parse string to " + typeWanted + " for \"" + colName + "\"; value : " + value
        );
    }
    return true;
}

field_type_t DefinitionFile::getColumnTypeWithColName(const std::string &colName) {
    for (const field &f: this->readDefinition().fieldsVector) {
        if (f.fieldName == colName) {
            return f.fieldType;
        }
    }
    throw ColumnNotInTableException("COLUMN < " + colName + " > WAS NOT FOUND");
}

std::string DefinitionFile::getFieldTypeNameByType(const field_type_t type) {
    if (type == INT) {
        return "int";
    } else if (type == PRIMARY_KEY) {
        return "primary key";
    } else if (type == FLOAT) {
        return "float";
    } else if (type == TEXT) {
        return "text (max 150)";
    } else {
        throw FieldTypeNotFoundException(&"Field type not found for the type "[type]);
    }
}
