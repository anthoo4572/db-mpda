#include "ManageFiles/Headers/KeyFile.h"

KeyFile::KeyFile(const string &p) : TableFile(Files::generateKeyFilePath(p)) {}

uint64_t KeyFile::getNextKey() {
    if (!this->getFile().is_open()) {
        this->open();
    }
    uint64_t test;
    this->getFile() >> test;
    this->close();
    return test;
}

void KeyFile::updateKey(uint64_t lastValue) {
    if (!this->getFile().is_open()) {
        this->open();
    }
    uint64_t new_value = lastValue + 1;
    this->getFile() << new_value;
    this->close();
}

void KeyFile::generateKeyFile() {
    if (!this->getFile().is_open()) {
        this->open();
    }
    this->getFile() << 1;
    this->close();
}

