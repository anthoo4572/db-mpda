//
// Created by matth on 22/01/2022.
//


#ifndef DB_MPDA_TABLEFILE_H
#define DB_MPDA_TABLEFILE_H

#include "Constants/Headers/types.h"
#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>
#include <filesystem>
#include "Exceptions/Headers/FileNotFoundException.h"

using namespace std;

class TableFile {
private:
    string sourcePath;
    std::fstream file;
public:
    TableFile();

    TableFile(string p);

    bool exists();

    void close();

    string getSourcePath();

    void open();

    fstream &getFile();
};


#endif //DB_MPDA_TABLEFILE_H
