#ifndef DB_MPDA_CONTENTFILE_H
#define DB_MPDA_CONTENTFILE_H

#include "Constants/Headers/types.h"
#include "TableFile.h"
#include "cstdint"
#include "Utils/Headers/Files.h"
#include "DefinitionFile.h"
#include "ManageFiles/Headers/IndexFile.h"


class ContentFile : public TableFile {
public:
    ContentFile(const string &p);

    void write_record(const std::vector<uint8_t> &record, uint32_t offset);

    std::vector<uint8_t> read_record(uint16_t length, uint32_t offset);

    vector<colValue>
    getValuesOfRecord(const uint32_t linePosition, const std::vector<field> &fields, const uint16_t lineLength);

    vector<vector<colValue>> getAllLineActive(const string &tableName);

    int getNumberOfLine(const string &tableName);

    void writeEndLineChar(const uint32_t offset);

    long long int getLastOffset();
};

#endif //DB_MPDA_CONTENTFILE_H
