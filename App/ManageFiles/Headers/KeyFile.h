#ifndef DB_MPDA_KEYFILE_H
#define DB_MPDA_KEYFILE_H

#include "Constants/Headers/types.h"
#include "TableFile.h"
#include "Utils/Headers/Files.h"

class KeyFile : public TableFile {
private:

public:
    KeyFile(const string &p);

    uint64_t getNextKey();

    void updateKey(uint64_t lastValue);

    void generateKeyFile();
};


#endif //DB_MPDA_KEYFILE_H
