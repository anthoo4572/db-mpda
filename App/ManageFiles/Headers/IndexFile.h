//
// Created by matth on 29/01/2022.
//

#ifndef DB_MPDA_INDEXFILE_H
#define DB_MPDA_INDEXFILE_H

#include "Constants/Headers/types.h"
#include <cstdint>
#include <string>
#include "TableFile.h"
#include "ManageFiles/Headers/TableFile.h"
#include "Exceptions/Headers/FieldTypeNotFoundException.h"
#include "Utils/Headers/Files.h"
#include <bitset>
#include <cmath>

class IndexFile : public TableFile {
public:
    IndexFile(const std::string &name);

    static int LINE_LENGTH;

    void writeIndexEntry(index_entry &entry);

    void updateIndexEntry(index_entry &entry, uint32_t offset);

    index_entry getIndexEntry(uint32_t position);

    tuple<index_entry, bool, int> getFirstUnactiveIndex();

    int getNumberOfLine();

};


#endif //DB_MPDA_INDEXFILE_H
