
#ifndef DB_MPDA_DEFINITIONFILE_H
#define DB_MPDA_DEFINITIONFILE_H

#include "Constants/Headers/types.h"
#include "TableFile.h"
#include <map>
#include "Exceptions/Headers/FieldTypeNotFoundException.h"
#include "Utils/Headers/Library.h"
#include "Utils/Headers/Files.h"
#include "Exceptions/Headers/CheckingSqlQueryException.h"
#include "Exceptions/Headers/ColumnNotInTableException.h"

class DefinitionFile : public TableFile {
private:
    table_definition tableDefinition;

    static int TEXT_LENGTH;

public :
    DefinitionFile(const std::string &name);

    table_definition get_table_definition();

    void set_table_definition(const table_definition &def);

    static int getFieldIdByName(const std::string &type);

    static field_type_t getFieldTypeByName(const string &type);

    void writeDefinition(const vector<field> &fields);

    table_definition readDefinition();

    static field_type_t getFieldTypeById(int number);

    static int getLengthOfType(field_type_t type);

    int getContentLength();

    static string getValueWithType(field_type_t type, const std::string &val);

    vector<string> getColumnNameByType(field_type_t type);

    static bool checkValueTypeWithColType(const string &colName, const string &value, const field_type_t type);

    field_type_t getColumnTypeWithColName(const string &colName);

    static string getFieldTypeNameByType(const field_type_t type);
};


#endif //DB_MPDA_DEFINITIONFILE_H
