#include "Exceptions/Headers/FieldTypeNotFoundException.h"

FieldTypeNotFoundException::FieldTypeNotFoundException(const std::string &msg) : BaseException(msg) {}
