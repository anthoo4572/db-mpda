#include "Exceptions/Headers/WhereOperatorNotFoundException.h"

WhereOperatorNotFoundException::WhereOperatorNotFoundException(const std::string &msg) : BaseException(
        "Operator " + msg + " NOT FOUND !") {}
