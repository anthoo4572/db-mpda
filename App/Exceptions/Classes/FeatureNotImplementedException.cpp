#include "Exceptions/Headers/FeatureNotImplementedException.h"

FeatureNotImplementedException::FeatureNotImplementedException() : BaseException(
        "This feature is not available with free tier. You have to buy a licence.") {}
