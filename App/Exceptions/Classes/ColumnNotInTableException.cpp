#include "Exceptions/Headers/ColumnNotInTableException.h"

ColumnNotInTableException::ColumnNotInTableException(const std::string &msg) : BaseException(msg) {}
