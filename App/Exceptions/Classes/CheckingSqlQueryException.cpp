#include "Exceptions/Headers/CheckingSqlQueryException.h"

CheckingSqlQueryException::CheckingSqlQueryException(const std::string &msg) : BaseException(msg) {}

CheckingSqlQueryException::CheckingSqlQueryException() :
        BaseException("CANNOT PARSE YOUR QUERY. PLEASE CHECK IT !") {
}
