#include "Exceptions/Headers/FileNotFoundException.h"

FileNotFoundException::FileNotFoundException(const std::string &msg) : BaseException(msg) {}
