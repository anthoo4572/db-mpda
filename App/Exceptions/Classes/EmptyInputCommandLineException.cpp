#include "Exceptions/Headers/EmptyInputCommandLineException.h"

EmptyInputCommandLineException::EmptyInputCommandLineException(const std::string &msg) : BaseException(msg) {}
