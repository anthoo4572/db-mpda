#include "Exceptions/Headers/TableNotExistsException.h"

TableNotExistsException::TableNotExistsException(const std::string &msg) : BaseException(msg) {}
