#include "Exceptions/Headers/LengthOfStringNotCorrectException.h"

LengthOfStringNotCorrectException::LengthOfStringNotCorrectException(const std::string &msg) : BaseException(msg) {}
