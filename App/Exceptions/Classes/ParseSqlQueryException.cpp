
#include "Exceptions/Headers/ParseSqlQueryException.h"

ParseSqlQueryException::ParseSqlQueryException(const std::string &msg) : BaseException(msg) {}
