#include "Exceptions/Headers/ExecutionSqlQueryException.h"

ExecutionSqlQueryException::ExecutionSqlQueryException(const std::string &msg) : BaseException(msg) {}
