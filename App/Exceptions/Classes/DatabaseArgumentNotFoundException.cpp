#include "Exceptions/Headers/DatabaseArgumentNotFoundException.h"

DatabaseArgumentNotFoundException::DatabaseArgumentNotFoundException(const std::string &msg) : BaseException(msg) {}
