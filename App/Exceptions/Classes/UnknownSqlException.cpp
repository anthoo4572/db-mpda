#include "Exceptions/Headers/UnknownSqlException.h"

UnknownSqlException::UnknownSqlException(const std::string &msg) : BaseException(msg) {}
