#include "Exceptions/Headers/BaseException.h"

BaseException::BaseException(std::string msg) : message(std::move(msg)) {}

const char *BaseException::what() const noexcept {
    return message.c_str();
}
