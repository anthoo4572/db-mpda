#include "Exceptions/Headers/TableAlreadyExistsException.h"

TableAlreadyExistsException::TableAlreadyExistsException(const std::string &msg) : BaseException(msg) {}
