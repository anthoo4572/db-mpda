#ifndef DB_MPDA_FIELDTYPENOTFOUNDEXCEPTION_H
#define DB_MPDA_FIELDTYPENOTFOUNDEXCEPTION_H

#include "BaseException.h"

class FieldTypeNotFoundException : public BaseException {
public:
    FieldTypeNotFoundException(const std::string &msg);
};


#endif //DB_MPDA_FIELDTYPENOTFOUNDEXCEPTION_H
