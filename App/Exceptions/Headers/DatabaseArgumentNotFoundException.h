#ifndef DB_MPDA_DATABASEARGUMENTNOTFOUNDEXCEPTION_H
#define DB_MPDA_DATABASEARGUMENTNOTFOUNDEXCEPTION_H

#include "BaseException.h"

class DatabaseArgumentNotFoundException : public BaseException {
public:
    DatabaseArgumentNotFoundException(const std::string &msg);
};


#endif //DB_MPDA_DATABASEARGUMENTNOTFOUNDEXCEPTION_H
