#ifndef DB_MPDA_BASEEXCEPTION_H
#define DB_MPDA_BASEEXCEPTION_H

#include <iostream>
#include <utility>
#include "exception"

class BaseException : public std::exception {
protected:
    std::string message;
public:
    explicit BaseException(std::string msg);

    virtual const char *what() const noexcept;
};

#endif //DB_MPDA_BASEEXCEPTION_H
