#ifndef DB_MPDA_EMPTYINPUTCOMMANDLINEEXCEPTION_H
#define DB_MPDA_EMPTYINPUTCOMMANDLINEEXCEPTION_H

#include "BaseException.h"

class EmptyInputCommandLineException : public BaseException {
public:
    EmptyInputCommandLineException(const std::string &msg);
};


#endif //DB_MPDA_EMPTYINPUTCOMMANDLINEEXCEPTION_H
