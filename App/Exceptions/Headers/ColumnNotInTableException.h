#ifndef DB_MPDA_COLUMNNOTINTABLEEXCEPTION_H
#define DB_MPDA_COLUMNNOTINTABLEEXCEPTION_H


#include "BaseException.h"

class ColumnNotInTableException  : public BaseException {
public:
    ColumnNotInTableException(const std::string &msg);
};


#endif //DB_MPDA_COLUMNNOTINTABLEEXCEPTION_H
