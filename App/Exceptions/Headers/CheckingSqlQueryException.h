#ifndef DB_MPDA_CHECKINGSQLQUERYEXCEPTION_H
#define DB_MPDA_CHECKINGSQLQUERYEXCEPTION_H


#include "BaseException.h"

class CheckingSqlQueryException : public BaseException {
public:
    CheckingSqlQueryException();

    explicit CheckingSqlQueryException(const std::string &msg);
};


#endif //DB_MPDA_CHECKINGSQLQUERYEXCEPTION_H
