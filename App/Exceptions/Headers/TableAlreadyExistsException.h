#ifndef DB_MPDA_TABLEALREADYEXISTSEXCEPTION_H
#define DB_MPDA_TABLEALREADYEXISTSEXCEPTION_H


#include "BaseException.h"

class TableAlreadyExistsException : public BaseException {
public:
    TableAlreadyExistsException(const std::string &msg);
};


#endif //DB_MPDA_TABLEALREADYEXISTSEXCEPTION_H
