#ifndef DB_MPDA_PARSESQLQUERYEXCEPTION_H
#define DB_MPDA_PARSESQLQUERYEXCEPTION_H


#include "BaseException.h"

class ParseSqlQueryException : public BaseException {
public:
    ParseSqlQueryException(const std::string &msg);
};


#endif //DB_MPDA_PARSESQLQUERYEXCEPTION_H
