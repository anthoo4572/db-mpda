#ifndef DB_MPDA_UNKNOWNSQLEXCEPTION_H
#define DB_MPDA_UNKNOWNSQLEXCEPTION_H


#include "BaseException.h"

class UnknownSqlException : public BaseException {
public:
    explicit UnknownSqlException(const std::string &msg);
};


#endif //DB_MPDA_UNKNOWNSQLEXCEPTION_H
