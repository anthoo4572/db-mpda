#ifndef DB_MPDA_ANDORINWHERECLAUSEEXCEPTION_H
#define DB_MPDA_ANDORINWHERECLAUSEEXCEPTION_H


#include "BaseException.h"

class AndOrInWhereClauseException : public BaseException {
public:
    AndOrInWhereClauseException();
};


#endif //DB_MPDA_ANDORINWHERECLAUSEEXCEPTION_H
