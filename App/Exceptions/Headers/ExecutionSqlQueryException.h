#ifndef DB_MPDA_EXECUTIONSQLQUERYEXCEPTION_H
#define DB_MPDA_EXECUTIONSQLQUERYEXCEPTION_H

#include "BaseException.h"

class ExecutionSqlQueryException : public BaseException{
public:
    ExecutionSqlQueryException(const std::string &msg);
};


#endif //DB_MPDA_EXECUTIONSQLQUERYEXCEPTION_H
