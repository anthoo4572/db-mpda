#ifndef DB_MPDA_WHEREOPERATORNOTFOUNDEXCEPTION_H
#define DB_MPDA_WHEREOPERATORNOTFOUNDEXCEPTION_H


#include "BaseException.h"

class WhereOperatorNotFoundException : public BaseException {
public:
    WhereOperatorNotFoundException(const std::string &msg);
};


#endif //DB_MPDA_WHEREOPERATORNOTFOUNDEXCEPTION_H
