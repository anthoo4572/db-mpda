#ifndef DB_MPDA_TABLENOTEXISTSEXCEPTION_H
#define DB_MPDA_TABLENOTEXISTSEXCEPTION_H

#include "BaseException.h"

class TableNotExistsException : public BaseException {
public:
    TableNotExistsException(const std::string &msg);
};


#endif //DB_MPDA_TABLENOTEXISTSEXCEPTION_H
