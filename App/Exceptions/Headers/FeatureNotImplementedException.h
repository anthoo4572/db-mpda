#ifndef DB_MPDA_FEATURENOTIMPLEMENTEDEXCEPTION_H
#define DB_MPDA_FEATURENOTIMPLEMENTEDEXCEPTION_H


#include "BaseException.h"

class FeatureNotImplementedException : public BaseException {
public:
    FeatureNotImplementedException();
};


#endif //DB_MPDA_FEATURENOTIMPLEMENTEDEXCEPTION_H
