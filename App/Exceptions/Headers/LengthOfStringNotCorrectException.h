#ifndef DB_MPDA_LENGTHOFSTRINGNOTCORRECTEXCEPTION_H
#define DB_MPDA_LENGTHOFSTRINGNOTCORRECTEXCEPTION_H


#include "BaseException.h"

class LengthOfStringNotCorrectException : public BaseException {
public:
    LengthOfStringNotCorrectException(const std::string &msg);
};


#endif //DB_MPDA_LENGTHOFSTRINGNOTCORRECTEXCEPTION_H
