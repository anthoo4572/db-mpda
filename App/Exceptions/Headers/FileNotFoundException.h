#ifndef DB_MPDA_FILENOTFOUNDEXCEPTION_H
#define DB_MPDA_FILENOTFOUNDEXCEPTION_H

#include "BaseException.h"
class FileNotFoundException : public BaseException{
public:
    FileNotFoundException(const std::string &msg);
};


#endif //DB_MPDA_FILENOTFOUNDEXCEPTION_H
