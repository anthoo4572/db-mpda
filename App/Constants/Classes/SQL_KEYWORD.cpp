#include "Constants/Headers/SQL_KEYWORD.h"

std::string SQL_KEYWORD::CREATE = "create";
std::string SQL_KEYWORD::INSERT = "insert";
std::string SQL_KEYWORD::SELECT = "select";
std::string SQL_KEYWORD::UPDATE = "update";
std::string SQL_KEYWORD::DELETE = "delete";
std::string SQL_KEYWORD::DROP = "drop";
std::string SQL_KEYWORD::TABLE = "table";
std::string SQL_KEYWORD::DATABASE = "database";
std::string SQL_KEYWORD::DB = "db";
std::string SQL_KEYWORD::DESCRIBE = "describe";
std::string SQL_KEYWORD::SHOW = "show";
std::string SQL_KEYWORD::TABLES = "tables";
