#include "Constants/Headers/SQL_REGEX.h"

std::string SQL_REGEX::COLUMN_TYPES = R"(int|float|primary key|text)";

std::string SQL_REGEX::CREATE_QUERY_ALL =
        R"(create table ([a-z]+)(\s)*\(((\s)*(,*)(\s)*([a-z]+(\s)*){1}(\s)+()" + SQL_REGEX::COLUMN_TYPES +
        R"(){1}(\s)*)+\))";

std::string SQL_REGEX::CREATE_QUERY_TABLE = R"(table ([a-z]+)(\s)*)";

std::string SQL_REGEX::CREATE_QUERY_ATTRIBUTES =
        R"(\(((\s)*(,*)(\s)*([a-z]+(\s)*){1}(\s)+()" + SQL_REGEX::COLUMN_TYPES +
        R"(){1}(\s)*)+\))";

std::string SQL_REGEX::OPERATOR_TYPES = "<=|>=|=|<|>|<>";

std::string SQL_REGEX::SELECT_WHERE = R"(where(\s)+[a-z]+(\s)*()" + SQL_REGEX::OPERATOR_TYPES +
                                      R"()(\s)*((([0-9])+(\.[0-9]+)?)|'([[:alnum:][:blank:][:punct:]]+)')(\s)*((\s)*(and|or)(\s)*[a-z]+(\s)*()" +
                                      SQL_REGEX::OPERATOR_TYPES +
                                      R"()(\s)*(([0-9])+(\.[0-9]+)?|'([[:alnum:][:blank:][:punct:]]+)'))*)";

std::string SQL_REGEX::SELECT_ALL =
        R"(select(\s)*(\*|([a-z]+(\s)*)(,(\s)*([a-z]+(\s)*))*)(\s)*from(\s)*[a-z]+(\s)*((\s)+)" + SELECT_WHERE +
        R"()*(\s)*)";

std::string SQL_REGEX::WHERE_ALL_PARTS = R"([a-z]+\s*()" + SQL_REGEX::OPERATOR_TYPES +
                                         R"()\s*'[[:alnum:][:blank:][:punct:]]+'|[a-z]+\s()" +
                                         SQL_REGEX::OPERATOR_TYPES +
                                         R"()\s*[0-9]+(\.[0-9]+)?)";

std::string SQL_REGEX::WHERE_PART = R"(([a-z]+)\s*()" + SQL_REGEX::OPERATOR_TYPES +
                                    R"()\s*('[[:alnum:][:blank:][:punct:]]+'|([0-9]+(\.[0-9]+)?)))";
std::string SQL_REGEX::INSERT_QUERRY_ALL =
        R"(insert *into *([a-z]+) *(\((([a-z]+ *\, *|[a-z]+)+ *)\))? *values *\((((\'([a-zA-Z0-9. ])+\' *\, *)|([0-9]+)(\.| *\, *))+(\'(([a-zA-Z0-9. ])+ *\')|([0-9]+\.?)+) *)\))";

std::string SQL_REGEX::INSERT_QUERRY_FIELD =
        R"(insert *into *([a-z]+) *\((([a-z]+ *\, *|[a-z]+)+ *)\))";

std::string SQL_REGEX::SELECT_TABLE = R"(from(\s)*([a-z]+)(\s)*)";

std::string SQL_REGEX::SELECT_ALL_ATTRIBUTES = R"(select(\s)*\*)";

std::string SQL_REGEX::DELETE_ALL = R"(delete(\s)*from(\s)*([a-z]+)(\s)*)";

std::string SQL_REGEX::DROP_TABLE = R"(drop table ([a-z]+)(\s)*)";

std::string SQL_REGEX::DROP_DATABASE = R"(drop database|db\s*)";

std::string SQL_REGEX::DESCRIBE_TABLE = R"(describe\s+([a-z]+)\s*)";