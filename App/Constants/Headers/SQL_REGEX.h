#ifndef DB_MPDA_SQL_REGEX_H
#define DB_MPDA_SQL_REGEX_H


#include <string>

class SQL_REGEX {
public:
    static std::string CREATE_QUERY_ALL;
    static std::string CREATE_QUERY_TABLE;
    static std::string CREATE_QUERY_ATTRIBUTES;
    static std::string COLUMN_TYPES;
    static std::string SELECT_ALL;
    static std::string SELECT_TABLE;
    static std::string SELECT_ALL_ATTRIBUTES;
    static std::string INSERT_QUERRY_ALL;
    static std::string INSERT_QUERRY_FIELD;
    static std::string INSERT_QUERRY_PARENTHESES;
    static std::string INSERT_QUERRY_TABLE;
    static std::string DELETE_ALL;
    static std::string SELECT_WHERE;
    static std::string WHERE_ALL_PARTS;
    static std::string WHERE_PART;
    static std::string OPERATOR_TYPES;
    static std::string DROP_TABLE;
    static std::string DROP_DATABASE;
    static std::string DESCRIBE_TABLE;
};


#endif //DB_MPDA_SQL_REGEX_H
