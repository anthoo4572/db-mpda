#ifndef DB_MPDA_TYPES_H
#define DB_MPDA_TYPES_H

#include <string>
#include <vector>

enum field_type_t {
    PRIMARY_KEY = 0,
    INT = 1,
    TEXT = 2,
    FLOAT = 3
};

enum whereOperator {
    EQUAL = 0,
    NOT_EQUAL = 1,
    LOWER_THAN = 2,
    LOWER_EQUAL_THAN = 3,
    GREATER_THAN = 4,
    GREATER_EQUAL_THAN = 5,
};

struct field {
    field_type_t fieldType;
    std::string fieldName;
    int length;
};

struct table_definition {
    std::vector<field> fieldsVector;
};

struct keyValue {
    std::string field;
    std::string value;
    field_type_t type;
};


struct whereOperation {
    std::string colName;
    whereOperator operatorr;
    std::string value;
};

struct colValue {
    std::string fieldName;
    std::string value;
};

struct __attribute__((__packed__)) index_entry {
    bool is_active;
    uint32_t position;
    uint16_t length;
};

typedef union {
    float f;
    struct {
        unsigned int mantissa: 23;
        unsigned int exponent: 8;
        unsigned int sign: 1;
    } raw;
} floatRepresentation;
#endif //DB_MPDA_TYPES_H
