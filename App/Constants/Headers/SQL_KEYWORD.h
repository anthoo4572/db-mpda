#ifndef DB_MPDA_SQL_KEYWORD_H
#define DB_MPDA_SQL_KEYWORD_H


#include <string>

class SQL_KEYWORD {
public:
    static std::string UPDATE;
    static std::string DB;
    static std::string DATABASE;
    static std::string CREATE;
    static std::string INSERT;
    static std::string SELECT;
    static std::string DELETE;
    static std::string DROP;
    static std::string TABLE;
    static std::string DESCRIBE;
    static std::string SHOW;
    static std::string TABLES;
};


#endif //DB_MPDA_SQL_KEYWORD_H
