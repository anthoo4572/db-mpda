### Lancer le programme : <br >

<code>db_mpda.exe -d database</code>

<code>db_mpda.exe -d databasetest -l C:\Users\profi\Desktop\DB</code>

### Afficher les tables

<code>show tables;</code>

### Créer une table

<code>create table employee (id primary key, age int, salary float, name text);</code>

### Afficher les tables

<code>show tables;</code>

### Décrire la table

<code>DESCRIBE employee;</code>

### Insérer un record

<code>insert IntO employeee values (1, 20, 1.3, 'toto');</code>

<code>insert IntO employee values (1, 20, 1.3, 'toto');</code>

<code>insert IntO employee values (21, 5.3, 'titi');</code>

<code>insert IntO employee values (1, 25, 1, 'tutu');</code>

<code>insert IntO employee values (5, 25, 1, tutu);</code>

<code>insert IntO employee values ('tt', 25, 1, 'tutu');</code>

<code>insert IntO employee (id, age, salary, name) values (10, 25, 1, 'llll');</code>

<code>insert IntO employee (age, salary, name) values (25, 1, 'llll');</code>

<code>insert IntO employee (age, salari, name) values (25, 1, 'llll');</code>

<code>insert IntO employee values (10, 45, 1, 'tutu', 'tt');</code>

<code>insert IntO employee (id, age, salary, name) values (10, 45, 1, 'tutu', 'tt');</code>

### Afficher les lignes

<code>select * from employeeee;</code>

<code>select * from employee;</code>

<code>select * from employee where id = 2;</code>

<code>select * from employee where id = 3;</code>

<code>select * from employee where id = 2 AND age = 21;</code>

<code>select * from employee where id = 2 AND age = 20;</code>

<code>select * from employee where id = 1 OR id = 2;</code>

<code>select * from employee where id = 1 OR id = 50;</code>

<code>select * from employee where id = 1 AND age = 20 OR age = 21;</code>

### Supprimer toutes les valeurs

<code>delete from employee;</code>

<code>select * from employee;</code>

<code>insert IntO employee values (1, 50, 45.90, 'tata');</code>

<code>select * from employee;</code>

### Modifier une ligne

<code>Update employee set name = 'test' WHERE id = 1; </code>

### Clear

<code>insert into employee () ; te </code>
<code>clear;</code>

### Exit

<code>exit();</code>

### Supprimer une table

<code>Drop table employee;</code>

<code>Show tables;</code>

### Supprimer la database

<code>Drop database;</code>

<code>db_mpda.exe -d database</code>

